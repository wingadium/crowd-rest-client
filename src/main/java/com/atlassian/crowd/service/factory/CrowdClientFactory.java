/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.service.factory;

import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;

/**
 * Class will create new instances of a CrowdClient.
 */
public interface CrowdClientFactory
{
    /**
     * Construct a new Crowd Client instance.
     *
     * @param url  URL of the remote Crowd server.
     * @param applicationName The application name of the connecting application.
     * @param applicationPassword The password of the connecting application.
     * @return new instance of CrowdClient
     */
    CrowdClient newInstance(final String url, final String applicationName, final String applicationPassword);

    /**
     * Constructs a new Crowd Client instance from the client properties.
     *
     * @param clientProperties all the properties needed to initialise the client.
     * @return new instance of CrowdClient
     */
    CrowdClient newInstance(final ClientProperties clientProperties);
}
