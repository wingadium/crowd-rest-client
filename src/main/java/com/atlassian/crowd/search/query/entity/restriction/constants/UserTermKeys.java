/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.query.entity.restriction.constants;

import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.google.common.collect.ImmutableSet;

import java.util.Date;
import java.util.Set;

/**
 * Attributes of a user.
 */
public class UserTermKeys
{
    public static final Property<String> USERNAME = new PropertyImpl<String>("name", String.class);
    public static final Property<String> EMAIL = new PropertyImpl<String>("email", String.class);
    public static final Property<String> FIRST_NAME = new PropertyImpl<String>("firstName", String.class);
    public static final Property<String> LAST_NAME = new PropertyImpl<String>("lastName", String.class);
    public static final Property<String> DISPLAY_NAME = new PropertyImpl<String>("displayName", String.class);
    public static final Property<Boolean> ACTIVE = new PropertyImpl<Boolean>("active", Boolean.class);
    public static final Property<String> EXTERNAL_ID = new PropertyImpl<String>("externalId", String.class);

    public static final Property<Date> CREATED_DATE = new PropertyImpl<Date>("createdDate", Date.class);
    public static final Property<Date> UPDATED_DATE = new PropertyImpl<Date>("updatedDate", Date.class);

    public static final Set<Property<?>> ALL_USER_PROPERTIES;

    static
    {
        ALL_USER_PROPERTIES = ImmutableSet.<Property<?>>of(
                USERNAME, EMAIL, FIRST_NAME, LAST_NAME, DISPLAY_NAME, ACTIVE, CREATED_DATE, UPDATED_DATE);
    }

    private UserTermKeys() {}
}
