/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.event;

import com.atlassian.crowd.model.event.OperationEvent;
import com.google.common.collect.Iterables;

public class Events
{
    private final Iterable<OperationEvent> events;

    private final String newEventToken;

    public Events(Iterable<OperationEvent> events, String newEventToken)
    {
        this.events = Iterables.unmodifiableIterable(events);
        this.newEventToken = newEventToken;
    }

    public Iterable<OperationEvent> getEvents()
    {
        return events;
    }

    public String getNewEventToken()
    {
        return newEventToken;
    }
}
