/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Thrown when the specified group could not be found.
 */
public class GroupNotFoundException extends ObjectNotFoundException
{
    private final String groupName;

    public GroupNotFoundException(String groupName)
    {
        this(groupName, null);
    }

    public GroupNotFoundException(String groupName, Throwable e)
    {
        super("Group <" + groupName + "> does not exist", e);
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
}
