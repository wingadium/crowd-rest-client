/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

import com.atlassian.crowd.model.webhook.Webhook;

/**
 * Thrown to indicate that a Webhook does not exist on the server
 *
 * @since v2.7
 */
public class WebhookNotFoundException extends ObjectNotFoundException
{
    public WebhookNotFoundException(long webhookId)
    {
        super(Webhook.class, "Webhook <" + webhookId + "> not found");
    }

    public WebhookNotFoundException(long applicationId, String endpointUrl)
    {
        super(Webhook.class, "applicationId=" + applicationId + ",endpointUrl=" + endpointUrl);
    }

    public WebhookNotFoundException(String message)
    {
        super(message);
    }
}
