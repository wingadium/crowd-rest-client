/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Used to denote that a particular USER-GROUP or GROUP-GROUP membership
 * does not exist.
 *
 * This could be thrown in cases where the calling code attempts to remove
 * a user from a group when the user is not a direct member of the group, etc.
 */
public class MembershipNotFoundException extends ObjectNotFoundException
{
    private final String childName;
    private final String parentName;

    public MembershipNotFoundException(String childName, String parentName)
    {
        this(childName, parentName, null);
    }

    public MembershipNotFoundException(String childName, String parentName, Throwable e)
    {
        super("The child entity <" + childName + "> is not a member of the parent <" + parentName + ">", e);
        this.childName = childName;
        this.parentName = parentName;
    }

    /**
     * Returns name of the child.
     *
     * @return name of the child
     */
    public String getChildName()
    {
        return childName;
    }

    /**
     * Returns names of the parent.
     *
     * @return name of the parent
     */
    public String getParentName()
    {
        return parentName;
    }
}

