/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.exception;

/**
 * Thrown when the account is inactive.
 */
public class InactiveAccountException extends FailedAuthenticationException
{
    private final String name;

    /**
     * Constructs a new InvalidAccountException.
     *
     * @param name name of the account
     */
    public InactiveAccountException(String name)
    {
        this(name, null);
    }

    public InactiveAccountException(String name, Throwable e)
    {
        super(String.format("Account with name <%s> is inactive", name), e);
        this.name = name;
    }

    /**
     * Returns the name of the account.
     *
     * @return name of the account
     */
    public String getName()
    {
        return name;
    }
}
