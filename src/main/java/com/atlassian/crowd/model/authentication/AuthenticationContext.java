/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.authentication;

import java.io.Serializable;
import java.util.Arrays;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.PasswordCredential;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Minimal information necessary when authenticating with the Crowd server.
 */
public abstract class AuthenticationContext implements Serializable
{
    private String name;
    private PasswordCredential credential;
    private ValidationFactor[] validationFactors;

    protected AuthenticationContext()
    {
    }

    protected AuthenticationContext(String name, @Nullable PasswordCredential credential, ValidationFactor[] validationFactors)
    {
        this.name = name;
        this.credential = checkNotEncrypted(credential);
        this.validationFactors = validationFactors;
    }

    private static PasswordCredential checkNotEncrypted(PasswordCredential credential)
    {
        if (credential != null && credential.isEncryptedCredential())
        {
            throw new IllegalArgumentException("Password credentials must not be encrypted");
        }
        else
        {
            return credential;
        }
    }

    /**
     * Gets the name of the authenticating entity.
     *
     * @return The name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of the authenticating entity.
     *
     * @param name The name.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the authenticating credential information.
     *
     * @return The credentials.
     */
    @Nullable
    public PasswordCredential getCredential()
    {
        return credential;
    }

    /**
     * Sets the authenticating credential information.
     *
     * @param credential The credentials.
     */
    public void setCredential(PasswordCredential credential)
    {
        this.credential = checkNotEncrypted(credential);
    }

    /**
     * Gets the authenticating validation factors.
     *
     * @return The validation factors.
     */
    public ValidationFactor[] getValidationFactors()
    {
        return validationFactors;
    }

    /**
     * Sets the authenticating validation factors.
     *
     * @param validationFactors The validation factors.
     */
    public void setValidationFactors(ValidationFactor[] validationFactors)
    {
        this.validationFactors = validationFactors;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationContext that = (AuthenticationContext) o;

        if (credential != null ? !credential.equals(that.credential) : that.credential != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (!Arrays.equals(validationFactors, that.validationFactors)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (credential != null ? credential.hashCode() : 0);
        result = 31 * result + (validationFactors != null ? Arrays.hashCode(validationFactors) : 0);
        return result;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("name", name).
                append("credential", credential).
                append("validationFactors", validationFactors).toString();
    }
}
