/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.webhook;

import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.application.Application;

/**
 * A Webhook is an application-provided HTTP endpoint that is pinged by Crowd to notify
 * the occurrence of certain events.
 *
 * @since v2.7
 */
public interface Webhook
{
    Long getId();

    String getEndpointUrl();

    Application getApplication();

    @Nullable
    String getToken();

    /**
     * @return Date of the last failed delivery that has not been followed by any successful delivery. May be null
     * if the last delivery was successful, or if no delivery has been attempted yet (i.e., new Webhooks).
     */
    @Nullable
    Date getOldestFailureDate();

    /**
     * @return Number of consecutive failed attempts to deliver the ping to the Webhook since the last successful
     * delivery, or since the Webhook was created. May be zero if the last delivery was successful, or if the Webhook
     * has just been created.
     */
    long getFailuresSinceLastSuccess();
}
