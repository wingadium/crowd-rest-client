/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.model.event;

import com.atlassian.crowd.embedded.api.Directory;

import java.util.Map;
import java.util.Set;

public abstract class AbstractAttributeEvent extends AbstractOperationEvent
{
    private final Map<String, Set<String>> storedAttributes;

    private final Set<String> deletedAttributes;

    public AbstractAttributeEvent(Operation operation, Directory directory, Map<String, Set<String>> storedAttributes, Set<String> deletedAttributes)
    {
        super(operation, directory);
        this.storedAttributes = storedAttributes;
        this.deletedAttributes = deletedAttributes;
    }

    public Map<String, Set<String>> getStoredAttributes()
    {
        return storedAttributes;
    }

    public Set<String> getDeletedAttributes()
    {
        return deletedAttributes;
    }
}
