/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.embedded.api;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Password validation request.
 */
public final class ValidatePasswordRequest
{
    private final PasswordCredential password;
    private final User user;

    /**
     * Creates a new ValidateRequest.
     *
     * @param password the (unencrypted) password to validate
     * @param user the User for whom to validate the password
     *
     * @throws java.lang.NullPointerException if password or user is null
     * @throws java.lang.IllegalArgumentException if password is encrypted
     */
    public ValidatePasswordRequest(PasswordCredential password, User user)
    {
        checkNotNull(password, "password");
        checkNotNull(user, "user");
        checkArgument(!password.isEncryptedCredential(), "password must not be encrypted");

        this.password = password;
        this.user = user;
    }

    /**
     * @return the (unencrypted) password to validate
     */
    public PasswordCredential getPassword()
    {
        return password;
    }

    /**
     * @return the User for whom to validate a password
     */
    public User getUser()
    {
        return user;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final ValidatePasswordRequest that = (ValidatePasswordRequest) o;

        if (!password.equals(that.password)) { return false; }
        if (!user.equals(that.user)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = password.hashCode();
        result = 31 * result + user.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "ValidatePasswordRequest{" +
                "password=" + password +
                ", user=" + user +
                '}';
    }
}
