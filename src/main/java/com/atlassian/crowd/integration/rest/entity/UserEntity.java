/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserComparator;
import com.atlassian.crowd.model.user.UserWithAttributes;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.*;
import java.util.Set;

/**
 * Represents a User entity (client side).
 *
 * @since v2.1
 */
@XmlRootElement (name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserEntity implements UserWithAttributes
{
    @XmlAttribute (name = "name")
    private final String name;

    @XmlElement (name = "first-name")
    private final String firstName;

    @XmlElement (name = "last-name")
    private final String lastName;

    @XmlElement (name = "display-name")
    private final String displayName;

    @XmlElement (name = "email")
    private final String emailAddress;

    @XmlElement (name = "password")
    private final PasswordEntity password; // will never be populated for a GET; only read for user creation or modification

    @XmlElement (name = "encrypted-password")
    private final PasswordEntity encryptedPassword; // will never be populated for a GET; only read for user creation or modification

    @XmlElement (name = "active")
    private final boolean active;

    @XmlElement (name = "key")
    private final String key;

    @XmlElement(name = "attributes")
    private MultiValuedAttributeEntityList attributes;

    /**
     * JAXB requires a no-arg constructor.
     */
    private UserEntity()
    {
        this(null, null, null, null, null, null, false, null);
    }

    public UserEntity(final String name, final String firstName, final String lastName, final String displayName, final String emailAddress, final PasswordEntity password, final boolean active, final String key)
    {
        this(name, firstName, lastName, displayName, emailAddress, password, active, key, false);
    }

    public UserEntity(final String name, final String firstName, final String lastName, final String displayName, final String emailAddress, final PasswordEntity password, final boolean active, final String key, boolean isPasswordEncrypted)
    {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = displayName;
        this.emailAddress = emailAddress;
        if(isPasswordEncrypted)
        {
            this.password = null;
            this.encryptedPassword = password;
        }
        else
        {
            this.password = password;
            this.encryptedPassword = null;
        }
        this.active = active;
        this.key = key;
    }

    public UserEntity(final String name, final String firstName, final String lastName, final String displayName, final String emailAddress, final PasswordEntity password, final boolean active)
    {
        this(name, firstName, lastName, displayName, emailAddress, password, active, null);
    }

    public UserEntity(final String name, final String firstName, final String lastName, final String displayName, final String emailAddress, final PasswordEntity password, final boolean active, boolean isPasswordEncrypted)
    {
        this(name, firstName, lastName, displayName, emailAddress, password, active, null, isPasswordEncrypted);
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public PasswordEntity getPassword()
    {
        return password;
    }

    public PasswordEntity getEncryptedPassword()
    {
        return encryptedPassword;
    }

    public boolean isActive()
    {
        return active;
    }

    public String getName()
    {
        return name;
    }

    public void setAttributes(final MultiValuedAttributeEntityList attributes)
    {
        this.attributes = attributes;
    }

    public MultiValuedAttributeEntityList getAttributes()
    {
        return attributes;
    }

    public Set<String> getValues(final String key)
    {
        return attributes.getValues(key);
    }

    public String getValue(final String key)
    {
        return attributes.getValue(key);
    }

    public Set<String> getKeys()
    {
        return attributes.getKeys();
    }

    public boolean isEmpty()
    {
        return attributes.isEmpty();
    }

    /**
     * @return always <code>0</code>
     * @deprecated this method has never returned the directoryId and its existence violates the directory
     * encapsulation. It will be removed in future versions.
     */
    @Deprecated
    public long getDirectoryId()
    {
        return 0L;
    }

    public int compareTo(final User user)
    {
        return UserComparator.compareTo(this, user);
    }

    /**
     * The user key at the server is the externalId at the client.
     */
    @Override
    public String getExternalId()
    {
        return key;
    }

    @Override
    public boolean equals(final Object o)
    {
        return UserComparator.equalsObject(this, o);
    }

    @Override
    public int hashCode()
    {
        return UserComparator.hashCode(this);
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("name", getName()).
                append("active", isActive()).
                append("emailAddress", getEmailAddress()).
                append("firstName", getFirstName()).
                append("lastName", getLastName()).
                append("displayName", getDisplayName()).
                append("externalId", getExternalId()).
                toString();
    }

    /**
     * Creates a new minimal user instance.
     *
     * @param username username for the user
     * @return minimal user instance
     */
    public static UserEntity newMinimalInstance(String username)
    {
        return new UserEntity(username, null, null, null, null, null, false, null);
    }
}
