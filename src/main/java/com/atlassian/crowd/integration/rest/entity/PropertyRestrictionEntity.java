/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a property restriction entity.
 */
@XmlRootElement (name = "property-search-restriction")
@XmlAccessorType (XmlAccessType.FIELD)
public class PropertyRestrictionEntity extends SearchRestrictionEntity
{
    @XmlElement (name = "property")
    private final PropertyEntity property;
    @XmlElement (name = "match-mode")
    private final String matchMode;
    @XmlElement (name = "value")
    private final String value;

    private PropertyRestrictionEntity()
    {
        property = null;
        matchMode = null;
        value = null;
    }

    /**
     * Creates a new instance of RestPropertyRestriction.
     *
     * @param property property to restrict on
     * @param matchMode property match mode
     * @param value value to match against
     * @return new instance of RestPropertyRestriction
     */
    public PropertyRestrictionEntity(final PropertyEntity property, final String matchMode, final String value)
    {
        this.property = property;
        this.matchMode = matchMode;
        this.value = value;
    }

    public PropertyEntity getProperty()
    {
        return property;
    }

    public String getMatchMode()
    {
        return matchMode;
    }

    public String getValue()
    {
        return value;
    }
}
