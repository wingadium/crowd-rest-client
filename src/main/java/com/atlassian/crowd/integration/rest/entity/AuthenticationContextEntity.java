/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import com.atlassian.crowd.model.authentication.UserAuthenticationContext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * REST version of an AuthenticationContext.
 */
@XmlRootElement (name = "authentication-context")
@XmlAccessorType (XmlAccessType.FIELD)
public class AuthenticationContextEntity
{
    @XmlElement (name = "username")
    private final String username;

    @XmlElement (name = "password")
    private final String password;

    @XmlElement (name = "validation-factors")
    private final ValidationFactorEntityList validationFactors;

    private AuthenticationContextEntity()
    {
        username = null;
        password = null;
        validationFactors = null;
    }

    public AuthenticationContextEntity(final String name, final String password, final ValidationFactorEntityList validationFactors)
    {
        this.username = name;
        this.password = password;
        this.validationFactors = validationFactors;
    }

    public String getUserName()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public List<ValidationFactorEntity> getValidationFactors()
    {
        if (validationFactors != null)
        {
            return validationFactors.getValidationFactors();
        }
        else
        {
            return Collections.emptyList();
        }
    }

    public static AuthenticationContextEntity newInstance(UserAuthenticationContext uac)
    {
        ValidationFactorEntityList validationFactorEntityList = ValidationFactorEntityList.newInstance(Arrays.asList(uac.getValidationFactors()));

        if (uac.getCredential() != null)
        {
            return new AuthenticationContextEntity(uac.getName(), uac.getCredential().getCredential(), validationFactorEntityList);
        }
        else
        {
            return new AuthenticationContextEntity(uac.getName(), null, validationFactorEntityList);
        }
    }
}
