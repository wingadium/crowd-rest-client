/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="events")
@XmlAccessorType(XmlAccessType.FIELD)
public class EventEntityList
{
    @XmlAttribute(name="newEventToken")
    private final String newEventToken;

    @XmlAttribute(name="incrementalSynchronisationAvailable")
    private final Boolean incrementalSynchronisationAvailable;

    @XmlElements({
            @XmlElement(name="userEvent", type=UserEventEntity.class),
            @XmlElement(name="groupEvent", type=GroupEventEntity.class),
            @XmlElement(name="userMembershipEvent", type=UserMembershipEventEntity.class),
            @XmlElement(name="groupMembershipEvent", type=GroupMembershipEventEntity.class)
    })
    private final List<AbstractEventEntity> events;

    /**
     * JAXB requires a no-arg constructor
     */
    private EventEntityList()
    {
        this.newEventToken = null;
        this.incrementalSynchronisationAvailable = null;
        this.events = null;
    }

    public String getNewEventToken()
    {
        return newEventToken;
    }

    public Boolean isIncrementalSynchronisationAvailable()
    {
        return incrementalSynchronisationAvailable;
    }

    public List<AbstractEventEntity> getEvents()
    {
        return events;
    }
}
