### Crowd REST Client
#### An Apache-licensed library for Crowd's REST API

[Atlassian Crowd](https://www.atlassian.com/software/crowd/) has an API consisting
of a number of [REST-based resources](https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+Resources).
This library simplifies access through Java.

#### Releases
Binary, source and documentation releases of this library are available from the
[Atlassian Public Maven Repository](https://developer.atlassian.com/display/DOCS/Atlassian+Maven+Repositories).
You can [query the repository](https://maven.atlassian.com/index.html#nexus-search;gav~com.atlassian.crowd.client~atlassian-crowd-rest-client~~~)
to find out which is the latest release. Add the dependency to your Maven pom.xml file as follows:

    <dependency>
      <groupId>com.atlassian.crowd.client</groupId>
      <artifactId>atlassian-crowd-rest-client</artifactId>
      <version>1.2</version>
    </dependency>

Or to your SBT build.sbt file projects as follows:

    resolvers += "atlassian-public" at "https://maven.atlassian.com/repository/public"

    libraryDependencies ++= Seq(
      "com.atlassian.crowd.client" % "atlassian-crowd-rest-client" % "1.2"
    )

(1.2 is just an example: check the repository for the latest release.)

#### Usage
You'll need Crowd's base URL and an application-specific name and password (see
[Adding an Application](https://confluence.atlassian.com/display/CROWD/Adding+an+Application)).

    import com.atlassian.crowd.service.client.CrowdClient;
    import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;

    CrowdClient client = new RestCrowdClientFactory().newInstance(url, applicationName, applicationPass);

For more information, please see the section on CrowdClient at
[Java Integration Libraries](https://developer.atlassian.com/display/CROWDDEV/Java+Integration+Libraries).

#### Compatibility
REST resources are supported since [Crowd 2.1](https://confluence.atlassian.com/display/CROWD/Crowd+2.1+Release+Notes),
although some resources may only be available in later versions.
